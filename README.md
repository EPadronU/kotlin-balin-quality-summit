# Globant's Quality Summit 2019
## Taking Advantage of Balin and Kotlin For Automating Tests of Web Applications

### How to Navigate the repository

1. [bare-metal](https://gitlab.com/EPadronU/kotlin-balin-quality-summit/tree/bare-metal)
2. [pom-intro](https://gitlab.com/EPadronU/kotlin-balin-quality-summit/tree/pom-intro)
3. [verification](https://gitlab.com/EPadronU/kotlin-balin-quality-summit/tree/verification)
4. [components](https://gitlab.com/EPadronU/kotlin-balin-quality-summit/tree/components)
5. [fluent](https://gitlab.com/EPadronU/kotlin-balin-quality-summit/tree/fluent)
6. [switching](https://gitlab.com/EPadronU/kotlin-balin-quality-summit/tree/switching)
7. [test](https://gitlab.com/EPadronU/kotlin-balin-quality-summit/tree/test)
8. [bdt](https://gitlab.com/EPadronU/kotlin-balin-quality-summit/tree/bdt)
9. [report](https://gitlab.com/EPadronU/kotlin-balin-quality-summit/tree/report)
10. [parallel](https://gitlab.com/EPadronU/kotlin-balin-quality-summit/tree/parallel)

[**Next branch >**](https://gitlab.com/EPadronU/kotlin-balin-quality-summit/tree/bare-metal)

### Notes

There's nothing to be ran in this branch.
